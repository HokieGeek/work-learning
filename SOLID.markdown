# What is solid?
The SOLID mnemonic describes design principles of software design that focuses on maintainable software

# Principles
## **S**ingle responsibility principle
An object should have a single responsibility.

## **O**pen/closed principle
Software modules should be "open for extension but closed for modification".

## **L**iskov substitution principle
Objects should be replaceable by their subtypes without altering the correctness of the program.

## **I**nterface segregation principle
It is better to have many client-specific interfaces than one general-purpose interface.

## **D**ependency inversion principle
Decouple your modules by ensuring that both high level modules and low level modules depend on abstractions of each other.
