# What are design patterns?
Design patterns are strategies for designing solutions to common problems.
These solutions are accepted in the industry as best practices.

## Suggested reading
See [AIC Book Recommendations](https://trac.cdpdf.net/confluence/display/AIC/Book+Recommendations)

# Some patterns we like to use

## Strategy pattern
### When to use
If your code will need to determine a specific algorithm at runtime, this pattern
suggests encapsulation of each algorithm in their own class or function and selecting
the proper one to execute at runtime.

### Description
### Example
[Java example from Wikipedia](https://en.wikipedia.org/wiki/Strategy_pattern#Java)

## Visitor pattern
### When to use
If your code has multiple algorithms which are determined at runtime, this pattern suggests how to encapsulate and use multiple groups of algorithms.

### Description

### Example
[Java example from Wikipedia](https://en.wikipedia.org/wiki/Visitor_pattern#Java_example)

## Builder
### When to use
In lieu of many constructors or one constructor/create method with many parameters,
an object which can take a large number of optional parameters can use this pattern
to clarify and simplify the creation process for clients.

### Description
This pattern describes an object which provides multiple ways of instantiating a different complex object.

### Example

## Chain of responsibility
### When to use
TODO: When you have logic which might be used by multiple objects

### Description
### Example

## Decorator (Wrapper)
### When to use
When you wish to add specific functionality to an instance of an object without extending the object's implementation.

### Description
A decorator object wraps around another object resulting in an object which provides the same interface as the original object but with additional features.

### Example

## Adapter
### When to use
### Description
### Example
